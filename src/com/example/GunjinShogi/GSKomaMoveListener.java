package com.example.GunjinShogi;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by fujisho on 2014/09/30.
 */
public class GSKomaMoveListener implements View.OnTouchListener {


    private final static int WC = RelativeLayout.LayoutParams.WRAP_CONTENT;
    private ImageView imageView;
    private static View parent;
    private RelativeLayout relativeLayout;
    Context mainContext;

    public GSKomaMoveListener(ImageView imageView, RelativeLayout layout, Context context) {
        this.imageView = imageView;
        parent = (View) imageView.getParent();
        this.relativeLayout = layout;
        mainContext = context;
    }



    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                ImageView cloneView = new ImageView(mainContext);

                cloneView.setImageResource(R.drawable.a);

                relativeLayout.addView(cloneView, new RelativeLayout.LayoutParams(WC, WC));



                break;
            default:
                break;
        }




        return true;
    }
}
