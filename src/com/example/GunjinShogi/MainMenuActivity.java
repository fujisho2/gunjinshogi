package com.example.GunjinShogi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by fujisho on 2014/10/09.
 */
public class MainMenuActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
    }



    public void pushArrangeButton(View v) {
        // インテントのインスタンス生成
        Intent intent = new Intent(this, ArrangeKomaActivity.class);
        // 次画面のアクティビティ起動
        startActivity(intent);
    }

    public void pushBattleButton(View v) {
        Intent intent = new Intent(this, Activity_1.class);
        startActivity(intent);
    }
}