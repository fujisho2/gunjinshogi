package com.example.GunjinShogi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by fujisho on 2014/10/05.
 * 駒を陣形を並び替えるアクティビティ
 * 2つのアクティビティから遷移
 * 1.MainMenuActivityから遷移
 *  並び替え，陣形の保存を行う
 *  TODO 陣形の保存数を増やす（現状1個のみ保存）
 * 2.Activity_1から遷移
 *  対戦直前の並び替えを行う
 *  1で保存された陣形を読込み表示している．
 *  保存はできない．
 *  TODO 1の保存数に連動して，読込数を増やす．保存できるようにする．
 *
 * 駒をタッチしたときの動作などはActivity_1とほぼ同じ
 */
public class ArrangeKomaActivity extends Activity implements View.OnTouchListener{

    private KomaView[] komaViews;
    private ImageView cloneView;
    private int cellWidth;
    private int oldx;
    private int oldy;
    private int currentX;
    private int currentY;

    private final int COL = Activity_1.COL;
    private final int CELL_SIZE = Activity_1.CELL_SIZE;
    private final int CELL_START = Activity_1.AREA_FRIEND;
    private final int HQ = Activity_1.HQ_FRIEND_RIGHT;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.arrengeview);

        komaViews = new KomaView[CELL_SIZE];

        // 保存されたデータがないと時に使用する駒の並び
        int typeList[] = {
                0, 1, 2, 3, 4, 5,
                6, 6, 7, 7, 8, 8,
                9, 9, 10, 10, 11, 15,
                12, 12, 13, -1, 14, 14
        };

        // 保存されたデータを読み込み
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("KOMA_1", Context.MODE_PRIVATE);
        int count = prefs.getInt("Count", 0);

        // 駒の初期配置
        for (int i = CELL_START; i < CELL_SIZE; i++) {
            int koma = 0;
            if (count == 0) {
                koma = typeList[i-CELL_START];
            } else {
                koma =  prefs.getInt("value_" + (i - CELL_START), i - CELL_START);
            }

            if (i == HQ) {
                komaViews[i] = new KomaView(this);
                komaViews[i].setType(koma);
                continue;
            }
            int resId = getResources().getIdentifier("cell_" + i, "id", this.getPackageName());
            komaViews[i] = (KomaView)findViewById(resId);
            komaViews[i].setOnTouchListener(this);
            komaViews[i].setTag(i);
            komaViews[i].setType(koma);

        }

        // 遷移元の情報を取得
        Intent intent = getIntent();
        int isBattle = intent.getIntExtra("activity", 0);

        // 対戦直前の処理
        if (isBattle == 1) {
            // 対戦スタートボタンの生成
            Button startBtn = (Button)findViewById(R.id.startbutton);
            startBtn.setVisibility(View.VISIBLE);

            Button saveBtn = (Button)findViewById(R.id.button);
            saveBtn.setVisibility(View.GONE);

            // 先攻後攻の表示
            String army = intent.getStringExtra("army");
            TextView textView = (TextView)findViewById(R.id.textView2);
            textView.setText("あなたが" + army + "です");
            textView.setVisibility(View.VISIBLE);

        }







        cloneView = (ImageView)findViewById(R.id.clone);
        cloneView.setAlpha(0.9f);




    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int x = (int) event.getRawX();
        int y = (int) event.getRawY();
        LinearLayout layout;
        int cell = (Integer)v.getTag();


        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:

                layout = (LinearLayout) v.getParent();
                currentX = v.getLeft();
                currentY = layout.getTop();

                cloneView.layout(currentX, currentY, currentX + cellWidth, currentY + cellWidth);
                cloneView.setVisibility(View.VISIBLE);

                break;
            case MotionEvent.ACTION_MOVE:
                currentX = cloneView.getLeft();
                currentY = cloneView.getTop();
                int left = currentX + x - oldx ;
                int top = currentY + y - oldy;

                cloneView.layout(left, top, left+ cellWidth, top+ cellWidth);
                break;
            case MotionEvent.ACTION_UP:
                cloneView.setVisibility(View.INVISIBLE);
                currentX = cloneView.getLeft();
                currentY = cloneView.getTop();

                int cell1 = cell;
                int cell2 = pointToCell(currentX, currentY);
                int type1 = komaViews[cell1].getType();
                int type2  = komaViews[cell2].getType();

                int resId = getResources().getIdentifier("k" + type1, "drawable", this.getPackageName());
                komaViews[cell2].setImageResource(resId);
                komaViews[cell2].setType(type1);
                resId = getResources().getIdentifier("k" + type2, "drawable", this.getPackageName());
                komaViews[cell1].setImageResource(resId);
                komaViews[cell1].setType(type2);

//                Log.d("aaa", "move cell: " + cell1 + " -> " + cell2 );
//                Log.d("aaa", "move type: " + type1 + " -> " + type2);

                break;

            default:

                break;
        }

        oldx = x;
        oldy = y;

        return true;
    }

    /**
     * 座標からセル番号を求める
     * @param x
     * @param y
     * @return
     */
    private int pointToCell (int x, int y){
        int cell = 0;

        int col = (int)Math.round((double)x / (double)cellWidth);
        int row = (int)Math.round((double)y / (double)cellWidth);

        cell = row * 6 + col;

        if (cell == 3 || cell == 51){
            cell--;
        }

        return cell;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        ImageView imageView = (ImageView)findViewById(R.id.cell_30);
        cellWidth = imageView.getHeight();
    }


    /**
     * 陣形を保存するためのボタン
     * TODO リストなどを使って複数保存できるようにする
     * @param view
     */
    public void pushSaveButton(View view) {

        ArrayList<Integer> komaList = new ArrayList<Integer>();

        for (int i = CELL_START; i < CELL_SIZE; i++){

            komaList.add(komaViews[i].getType());
//            Log.d("aaa", "save " + komaViews[i].getType());
        }

        storeIntegerArrayList(komaList, "KOMA_1", "value_");
        finish();
    }


    /**
     * 対戦開始ボタン
     * 並び替えた陣形を遷移元に送る
     * @param v
     */
    public void pushStartButton(View v) {
        ArrayList<Integer> komaList = new ArrayList<Integer>();

        for (int i = CELL_START; i < CELL_SIZE; i++){
            komaList.add(komaViews[i].getType());
        }

        // intentの作成
        Intent intent = new Intent();
        // intentへ添え字付で値を保持させる
        intent.putExtra("koma", komaList);
        // 返却したい結果ステータスをセットする
        setResult( Activity.RESULT_OK, intent );
        // アクティビティを終了させる
        finish();


    }


    /**
     * ArrayList<Integer>を保存
     * @param arrayList
     * @param key
     * @param value
     */
    public void storeIntegerArrayList(ArrayList<Integer> arrayList, String key, String value){
        SharedPreferences.Editor edit= getApplicationContext().getSharedPreferences(key, Context.MODE_PRIVATE).edit();
        edit.putInt("Count", arrayList.size());
        int count = 0;
        for (int i: arrayList){
            edit.putInt(value + count++, i);
        }
        edit.commit();
    }
}