package com.example.GunjinShogi;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Message;
import android.os.Handler;
import android.util.Log;
import android.view.*;
import android.widget.*;


import org.apache.commons.collections4.*;

import java.util.ArrayList;

/**
 * Created by fujisho on 2014/10/09.
 *
 * マッチングを行う画面(connect_view.xml)
 * 対戦を行う画面(main.xml)
 *
 */

public class Activity_1 extends Activity implements View.OnTouchListener {
    /**
     * Called when the activity is first created.
     */

    private final static int WC = RelativeLayout.LayoutParams.WRAP_CONTENT;
    private final static int MP = ViewGroup.LayoutParams.MATCH_PARENT;

    public static final int COL = 6;
    public static final int ROW = 9;
    public static final int CELL_SIZE = COL * ROW;
    public static final int TYPE_ROAD = 20;                     //
    public static final int TYPE_GUNKI = 15;
    public static final int HQ_ENEMY_RIGHT = COL / 2;                       //敵軍司令部の2マス目(3)
    public static final int HQ_ENEMY_LEFT = HQ_ENEMY_RIGHT - 1;             //敵軍司令部の1マス目(2)
    public static final int HQ_FRIEND_RIGHT = CELL_SIZE - HQ_ENEMY_RIGHT;   //友軍司令部の2マス目(51)
    public static final int HQ_FRIEND_LEFT = HQ_FRIEND_RIGHT - 1;           //友軍司令部の1マス目(50)
    public static final int AREA_FRIEND = 30;                   //友軍陣地(30~53)
    public static final int AREA_ENEMY = 23;                    //敵軍陣地(0~23)
    public static final int BATTLE_START = 1000;
    public static final int IS_FRIEND = 1;
    public static final int IS_ENEMY = 0;
    public static final int IS_ROAD = TYPE_ROAD;

    private boolean readyBattleField = false;                   //盤面が生成できているか
    private boolean readyEnemy = false;                         //敵軍の駒が配置できているか
    public static boolean isFirst = false;                      //先攻・後攻
    private boolean isTurn = false;                             //自分のターン

    public static final int MESSAGE_OPTION_BATTLE = 0;          //対戦情報の通信
    public static final int MESSAGE_OPTION_ENEMIES = 1;         //敵軍の駒配置の通信

    private KomaView[] komaViews;                               //盤面の駒を表すクラス
    private ImageView cloneView;                                //駒をつかんだときに表示する駒のクローン
    private static RelativeLayout relativeLayout;
    private int cellWidth;
    private int oldx;
    private int oldy;
    private int currentX;
    private int currentY;

    // つかんだ駒の移動可能なマスのリスト
    public ArrayList<Integer> movableCellList = new ArrayList<Integer>();
    // 使わない(KomaViewクラスと統合済み)
    public ArrayList<Integer> friendsCellList = new ArrayList<Integer>();
    // 使わない(KomaViewクラスと統合済み)
    public ArrayList<Integer> enemiesCellList = new ArrayList<Integer>();
    // getMovableCellListで使う
    public ArrayList<Integer> allOfCellList = new ArrayList<Integer>();

    private ArrayList<Integer> enemiesInitList = new ArrayList<Integer>();

    // 駒の勝敗表 軍旗については，駒の配置後に書き換える．
    private int strengthChart[][] = {
            { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 0,-1}, // 0 大将
            {-1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,-1}, // 1 中将
            {-1,-1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,-1}, // 2 少将
            {-1,-1,-1, 0, 1, 1, 1, 1, 1,-1,-1, 1, 1, 1, 0,-1}, // 3 大佐
            {-1,-1,-1,-1, 0, 1, 1, 1, 1,-1,-1, 1, 1, 1, 0,-1}, // 4 中佐
            {-1,-1,-1,-1,-1, 0, 1, 1, 1,-1,-1, 1, 1, 1, 0,-1}, // 5 少佐
            {-1,-1,-1,-1,-1,-1, 0, 1, 1,-1,-1, 1, 1, 1, 0,-1}, // 6 大尉
            {-1,-1,-1,-1,-1,-1,-1, 0, 1,-1,-1, 1, 1, 1, 0,-1}, // 7 中尉
            {-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1, 1, 1, 1, 0,-1}, // 8 少尉
            {-1,-1,-1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,-1}, // 9 飛行機
            {-1,-1,-1, 1, 1, 1, 1, 1, 1,-1, 0, 1,-1, 1, 0,-1}, //10 タンク
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 1, 1, 0,-1}, //11 騎兵
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1,-1, 0, 1, 1,-1}, //12 工兵
            { 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0,-1}, //13 スパイ
            { 0, 0, 0, 0, 0, 0, 0, 0, 0,-1, 0, 0,-1, 0, 0,-1}, //14 地雷
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}  //15 軍旗
    };


    /* bluetooth */
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Member object for the chat services
    private BluetoothService bluetoothService = null;




    /* debug */
    int initlist[] = { 0, 1, 2, -1, 3, 4, 5,
            6, 6, 7, 7, 8, 8,
            9, 9, 10, 10, 11, 15,
            12, 12, 13, 14, 14};




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        komaViews = new KomaView[CELL_SIZE];

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (bluetoothService == null)
                setupBluetooth();
        }


        // ウィンドウマネージャのインスタンス取得
        WindowManager wm = (WindowManager)getSystemService(WINDOW_SERVICE);
        // ディスプレイのインスタンス生成
        Display disp = wm.getDefaultDisplay();
        Point size = new Point();
        disp.getSize(size);
        cellWidth = size.x / COL;



        setContentView(R.layout.connect_view);

    }


    /**
     * 盤面を生成するメソッド
     *
     */
    private void initializeFieldOfView(){
        setContentView(R.layout.main);

        relativeLayout = (RelativeLayout) findViewById(R.id.flame);
        relativeLayout.setBackgroundColor(Color.rgb(169, 121, 70));



        for (int i = 0; i < CELL_SIZE; i++) {
            if (i == HQ_FRIEND_RIGHT || i == HQ_ENEMY_RIGHT) {
                // 司令部は2セル分
                continue;
            }

            int resId = getResources().getIdentifier("cell_" + i, "id", this.getPackageName());
            komaViews[i] = (KomaView) findViewById(resId);
            komaViews[i].setTag(i);
            komaViews[i].setColorFilter(Color.argb(0, 0, 0, 0));
            komaViews[i].setBackgroundColor(Color.argb(0, 0, 0, 0));
            komaViews[i].setOnTouchListener(this);

            komaViews[i].setArmy(IS_ROAD);
            komaViews[i].setType(TYPE_ROAD);
        }
        cloneView = (ImageView) findViewById(R.id.clone);
        cloneView.setAlpha(0.9f);

        readyBattleField = true;
        if (readyEnemy){
            arrangeEnemies(enemiesInitList);
            if (isFirst) {
                Toast.makeText(getApplicationContext(), "あなたのターンです", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "あいてのターンです", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * 敵軍の駒を配置するメソッド
     * @param typeList
     */
    private void arrangeEnemies(ArrayList<Integer> typeList) {
        int gunki = 0;

        int hq = typeList.size() - HQ_ENEMY_RIGHT; //21
        int tmp = typeList.get(hq);
        typeList.set(hq, typeList.get(hq-1));
        typeList.set(hq-1, tmp);

        for (int i = 0; i <= AREA_ENEMY ; i++) {
            if (i == HQ_ENEMY_RIGHT) {
                continue;
            }

            int type = typeList.get(AREA_ENEMY - i);
            // 軍旗の場所を保存
            if (type == TYPE_GUNKI) {
                gunki = i;
            }
            int resId = getResources().getIdentifier("kk", "drawable", this.getPackageName());
            komaViews[i].setType(type);
            komaViews[i].setArmy(IS_ENEMY);
            komaViews[i].setImageResource(resId);
            enemiesCellList.add(i);
            allOfCellList.add(i);
        }

        /* 軍旗をその後ろの駒と同じ強さにする */
        if (gunki + COL == HQ_ENEMY_RIGHT) {
            // 後ろの駒が司令部
            gunki = HQ_FRIEND_RIGHT - 1;
        } else if (gunki > COL) {
            //
            gunki = gunki - COL;
        }
        gunki = komaViews[gunki].getType();
        for (int i = 0; i < strengthChart.length; i++) {
            strengthChart[TYPE_GUNKI][i] = strengthChart[gunki][i];
        }

    }

    /**
     * 友軍の駒を配置するメソッド
     * @param typeList
     */
    private void arrangeFriends(ArrayList<Integer> typeList) {
        for (int i = 0; i < CELL_SIZE; i++) {
            friendsCellList.add(i, -1);
        }
        int gunki = 0;

        for (int i = AREA_FRIEND; i < CELL_SIZE; i++) {
            if (i == HQ_FRIEND_RIGHT) {
                continue;
            }

            int type = typeList.get(i - AREA_FRIEND);

            // 軍旗の場所を保存
            if (type == TYPE_GUNKI) {
                gunki = i;
            }
            int resId = getResources().getIdentifier("k" + type, "drawable", this.getPackageName());
            komaViews[i].setType(type);
            komaViews[i].setArmy(IS_FRIEND);
            komaViews[i].setImageResource(resId);
            friendsCellList.add(i);
            allOfCellList.add(i);
        }

        /* 軍旗をその後ろの駒と同じ強さにする */
        if (gunki + COL == HQ_FRIEND_RIGHT) {
            // 後ろの駒が司令部
            gunki = HQ_FRIEND_RIGHT - 1;
        } else if (gunki < CELL_SIZE - COL - 1) {
            //
            gunki = gunki + COL;
        }
        gunki = komaViews[gunki].getType();
        for (int i = 0; i < strengthChart.length; i++) {
            strengthChart[TYPE_GUNKI][i] = strengthChart[gunki][i];
        }
    }


    /**
     * 駒をタッチしたときに呼び出されるメソッド
     * ACTION_DOWN: 駒にタッチ(つかむ)したとき，その駒の移動可能マスを計算し，ハイライトする．
     * ACTION_MOVE: 駒をドラッグしたとき，駒のクローンが動く．
     * ACTION_UP  : 駒を離したとき，移動可能なマスなら移動および戦闘．
     * @param v
     * @param event
     * @return
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int x = (int) event.getRawX();
        int y = (int) event.getRawY();
        LinearLayout layout;
        // タッチした駒のセル番号
        int cell = (Integer)v.getTag();

        KomaView koma = (KomaView)v;

        if (!isTurn) {
            return true;
        }

        // 友軍の駒以外触れられない
        if (koma.getArmy() != IS_FRIEND) {
            Log.d("aaa", "touch is not friend.");
            return true;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                layout = (LinearLayout) v.getParent();
                currentX = v.getLeft();
                currentY = layout.getTop();

                // 全セルを暗く
                adumbrateAllCell();
                // 移動可能なセルを明るく
                movableCellList = null;
                movableCellList = getLightUpCellList(cell, koma.getMovableType());
                lightUpCell(movableCellList);

                // クローンを生成
                cloneView.layout(currentX, currentY, currentX + cellWidth, currentY + cellWidth);
                cloneView.setVisibility(View.VISIBLE);

                break;
            case MotionEvent.ACTION_MOVE:
                currentX = cloneView.getLeft();
                currentY = cloneView.getTop();
                int left = currentX + x - oldx ;
                int top = currentY + y - oldy;
                cloneView.layout(left, top, left+ cellWidth, top+ cellWidth);
                break;
            case MotionEvent.ACTION_UP:
                // ハイライトを解除
                lightUpAllCell();
                // クローンを削除(不可視化)
                cloneView.setVisibility(View.INVISIBLE);

                currentX = cloneView.getLeft();
                currentY = cloneView.getTop();

                //移動前のマス
                int from = cell;
                //移動後のマス
                int to = pointToCell(x, currentY);

                // 画面外へのドロップ
                if (to == -1) {
                    break;
                }
                // 移動前のマスにあった駒(動かす駒)
                int friend = koma.getType();
                // 移動後のマスの駒
                int enemy  = komaViews[to].getType();

                // 実際に移動可能なマスのリストを取得
                movableCellList =  getMovableCellList(movableCellList, koma.getMovableType(), from);

                // ドロップしたセルが可動範囲かつ友軍でない時
                if ((movableCellList.indexOf(to) != -1) && (komaViews[to].getArmy() != IS_FRIEND)) {
                    // 駒の勝敗判定
                    int judge = judge(friend, enemy);
                    // 駒の対戦
                    battle(friend, from, to, judge, IS_FRIEND);
                    // 対戦情報を相手に送信
                    String msg = MESSAGE_OPTION_BATTLE + "/" + judge + "/" + friend + "/" + from + "/" + to;
                    sendMessage(msg);
                    // 相手のターンになる
                    isTurn = false;
                    Toast.makeText(getApplicationContext(), "あいてのターンです", Toast.LENGTH_SHORT).show();

                }
                break;
            default:
                break;
        }

        oldx = x;
        oldy = y;

        return true;
    }

    /**
     * 対戦(後の処理)を行うメソッド
     * TODO アニメーション
     * @param koma  攻駒の種類
     * @param from  移動前のマス
     * @param to    移動後のマス
     * @param judge 勝敗
     * @param army  攻駒が友軍か敵軍か
     */
    private void battle(int koma, int from, int to, int judge, int army) {

        // 攻駒が敵軍の場合，マス情報を反転
        if (army == IS_ENEMY) {
            from = CELL_SIZE - 1 - from;
            to = CELL_SIZE - 1 - to;
        }
        // 司令部の2マス目の処理
        if (to == HQ_FRIEND_RIGHT || to == HQ_ENEMY_RIGHT) {
            to--;
        }

        switch (judge) {
            // 負け
            case -1:
                // 自分を破壊
                delete(from);
                break;
            // 引き分け
            case 0:
                // 互いを破壊
                delete(from);
                delete(to);
                break;
            // 勝ち
            case 1:
                // 移動
                move(koma, from, to, army);
                break;
            // 道
            case TYPE_ROAD:
                // 移動
                move(koma, from, to, army);
                break;
            default:
                break;
        }

    }


    /**
     * 画面が表示された時に呼び出されるメソッド
     * Viewのサイズを取得したりする．
     * @param hasFocus
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    /**
     * 移動先は必ず敵軍もしくは道である．
     * 司令部に尉官以上が移動したらゲーム終了．
     * hogeCellListは使ってない，
     * @param type 駒の種類
     * @param from 移動元のセル
     * @param to   移動先のセル
     * @param army 移動した駒の属する軍
     */
    private void move(int type, int from, int to, int army){
        // 移動元のセルに道を配置
        komaViews[from].setArmy(IS_ROAD);
        komaViews[from].setType(TYPE_ROAD);
        // 移動先のセルに駒を配置
        komaViews[to].setType(type);
        komaViews[to].setArmy(army);
        // 移動元のセルをセルリストから削除
        friendsCellList.remove(new Integer(from));
        allOfCellList.remove(new Integer(from));
        // 移動先のセルをセルリストから削除
        enemiesCellList.remove(new Integer(to));
        allOfCellList.remove(new Integer(to));
        // 移動先のセルをセルリストに追加
        friendsCellList.add(to);
        allOfCellList.add(to);

        // 移動元の画像を消す
        komaViews[from].setImageBitmap(null);
        // 移動先の画像を更新
        int resId = 0;
        if (army == IS_FRIEND) {
            resId = getResources().getIdentifier("k" + type, "drawable", this.getPackageName());
        } else {
            resId = getResources().getIdentifier("kk", "drawable", this.getPackageName());
        }
        komaViews[to].setImageResource(resId);

        // 友軍勝利
        if (army == IS_FRIEND && (to == HQ_ENEMY_RIGHT || to == HQ_ENEMY_RIGHT - 1) && (type < 9)){
            finishGame("あなたの勝ちです");
        }
        // 敵軍勝利
        if (army == IS_ENEMY && (to == HQ_FRIEND_RIGHT || to == HQ_FRIEND_RIGHT - 1) && (type < 9)){
            finishGame("あなたの負けです");
        }



    }




    /**
     * cell上の駒を破壊するメソッド
     * 破壊したセルは道にする．
     * @param cell
     */
    private void delete(int cell) {
        friendsCellList.remove(new Integer(cell));
        enemiesCellList.remove(new Integer(cell));
        allOfCellList.remove(new Integer(cell));
        komaViews[cell].setArmy(IS_ROAD);
        komaViews[cell].setType(TYPE_ROAD);
        komaViews[cell].setImageBitmap(null);
    }

    /**
     * 駒をタッチしたとき，ライトアップのために理論的な可動領域を求める．
     *
     * @param cell
     * @param type
     * @return
     */
    private ArrayList<Integer> getLightUpCellList(int cell, int type) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();

        int col = cell % COL;
        int row = cell / COL;

        ArrayList<Integer> tempList =  new ArrayList<Integer>();
        // 隣接するセルの集合を求める
        tempList.add(cell);     // 自分のマスを追加
        tempList.add(cell-COL); // 前のマスを追加
        tempList.add(cell-1);   // 左のマスを追加
        tempList.add(cell+1);   // 右のマスを追加
        tempList.add(cell+COL); // 後のマスを追加

        // 司令部の1マス目の時，2マス目も追加
        if (cell == HQ_ENEMY_RIGHT - 1 || cell == HQ_FRIEND_RIGHT - 1) {
            tempList.addAll(getLightUpCellList(cell + 1, type));
        }

        /* 駒の種類に応じて処理 */
        switch (type) {
            /**
             * 前後左右1駒 ： 尉官以上，スパイ
             * 同列同行のセルの集合と，隣接するセルの集合の積集合によって求める
             */
            case 1:
                arrayList = verticalCells(cell, col, row);
                arrayList.addAll(horizontalCells(cell, col, row));

                arrayList = (ArrayList<Integer>) CollectionUtils.intersection(arrayList, tempList);
                break;
            /**
             * 前後 ： 飛行機
             * 同列のセルの集合
             */
            case 2:
                arrayList = verticalCells(cell, col, row);
                break;
            /**
             * 前後左右2駒 ： タンク，騎兵
             * 同行同列のセルの集合と，隣接2マスのセルの集合の積集合
             */
            case 3:
                arrayList = verticalCells(cell, col, row);
                arrayList.addAll(horizontalCells(cell, col, row));
                // 隣接2マス
                tempList.add(cell-(COL*2)); // 2マス前
                tempList.add(cell-2);       // 2マス左
                tempList.add(cell+2);       // 2マス右
                tempList.add(cell+(COL*2)); // 2マス後

                // 司令部周りの処理
                if (cell == HQ_ENEMY_RIGHT - 2 || cell == HQ_FRIEND_RIGHT - 2) {
                    tempList.add(cell+3);
                }
                if (cell == HQ_ENEMY_RIGHT + 1 || cell == HQ_FRIEND_RIGHT + 1) {
                    tempList.add(cell-3);
                }

                arrayList = (ArrayList<Integer>) CollectionUtils.intersection(arrayList, tempList);
                break;
            /**
             * 前後左右：工兵
             * 同行同列のセル集合
             */
            case 4:
                arrayList = verticalCells(cell, col, row);
                arrayList.addAll(horizontalCells(cell, col, row));
            default:
                break;

        }

        if (arrayList.indexOf(HQ_ENEMY_RIGHT) != -1) {
            arrayList.add(HQ_ENEMY_RIGHT - 1);
        }
        if (arrayList.indexOf(HQ_FRIEND_RIGHT) != -1) {
            arrayList.add(HQ_FRIEND_RIGHT - 1);
        }

        return arrayList;

    }

    /**
     * 実際に移動可能なマスのリストを求めるメソッド
     * mtype=3,4の駒は2マス以上動けるが，駒を飛び越えることはできない．
     * 理論的に動けるマスの集合と同行同列の駒が存在しないマスの集合との積集合
     * @param arrayList
     * @param mtype
     * @return
     */
    private ArrayList<Integer> getMovableCellList(ArrayList<Integer> arrayList, int mtype, int cell) {
        ArrayList<Integer> tempList = new ArrayList<Integer>();

        // もといたマスには移動できない
        arrayList.remove(new Integer(cell));

        if (mtype < 3) {
            return arrayList;
        }

        /**
         * 前後左右1マス分ずつチェックしていき，駒のないマスをtempListに追加．
         * 駒が存在するマスだったらbreak
         */
        // 前の駒を調べる
        for (int i = 1; i < ROW; i++) {
            int t = cell - (COL * i);
            tempList.add(t);
            // 駒が存在
            if (allOfCellList.indexOf(t) != -1) {

                break;
            }
        }
        // 後の駒を調べる
        for (int i = 1; i < ROW; i++) {
            int d = cell + (COL * i);
            tempList.add(d);
            if (allOfCellList.indexOf(d) != -1) {

                break;
            }
        }
        // 左の駒を調べる
        for (int i = 1; i < ROW; i++) {
            int l = cell - (i);
            tempList.add(l);
            if (allOfCellList.indexOf(l) != -1) {

                break;
            }
        }
        // 右の駒を調べる
        for (int i = 1; i < ROW; i++) {
            int r = cell + (i);
            tempList.add(r);
            if (allOfCellList.indexOf(r) != -1) {

                break;
            }
        }

        return (ArrayList<Integer>) CollectionUtils.intersection(arrayList, tempList);
    }


    /**
     * cellと同列のセルを返す
     * cellが司令部なら2列分のセルを返す
     * @param cell
     * @param col
     * @param row
     * @return
     */
    private ArrayList<Integer> verticalCells(int cell, int col, int row) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        int mcell = 0;

        if (cell == HQ_ENEMY_LEFT || cell == HQ_FRIEND_LEFT) {
            arrayList.add(HQ_ENEMY_LEFT);
            arrayList.add(HQ_FRIEND_LEFT);
            for (int i = 1; i < ROW -1; i++) {
                mcell = i * COL + col;
                arrayList.add(mcell);
                arrayList.add(mcell+1);
            }
        } else {
            for (int i = 0; i < ROW; i++) {
                mcell = i * COL + col;
//                if (mcell == 3 || mcell == 51) {
//                    mcell--;
//                }
                arrayList.add(mcell);

            }
        }
        return arrayList;
    }

    /**
     * cellと同行のセルを返す
     * @param cell
     * @param col
     * @param row
     * @return
     */
    private ArrayList<Integer> horizontalCells(int cell, int col, int row){
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        int mcell = 0;
        for (int i = 0; i < COL; i++) {
            mcell = i + COL * row;
                arrayList.add(mcell);
        }

        return arrayList;
    }


    /**
     * 今は使っていない．
     * @param cell
     * @param col
     * @param row
     * @return
     */
    private ArrayList<Integer> oneOfAllDirections(int cell, int col, int row){
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(cell);
        int mcell = 0;
        if (cell == 50) {
            arrayList.add(44);
            arrayList.add(45);
            arrayList.add(49);
            arrayList.add(52);
        } else if (cell == 2) {
            arrayList.add(1);
            arrayList.add(4);
            arrayList.add(8);
            arrayList.add(9);
        } else {
            // 左端以外
            if (col != 0) {
                mcell = cell - 1;
                if (mcell == 3 || mcell == 51) {
                    mcell--;
                }
                arrayList.add(mcell);
            }
            // 右端以外
            if (col != COL - 1) {
                mcell = cell + 1;
                if (mcell == 3 || mcell == 51) {
                    mcell--;
                }
                arrayList.add(mcell);
            }
            // 上端以外
            if (row != 0) {
                mcell = cell - 6;
                if (mcell == 3 || mcell == 51) {
                    mcell--;
                }
                arrayList.add(mcell);
            }
            // 下端以外
            if (row != ROW - 1) {
                mcell = cell + 6;
                if (mcell == 3 || mcell == 51) {
                    mcell--;
                }
                arrayList.add(mcell);
            }
        }
        return  arrayList;
    }


    /**
     * 盤面を暗くするメソッド
     */
    private void adumbrateAllCell(){
        for (int i = 0; i < CELL_SIZE; i++){
            if (i == 3 || i == 51) {
                continue;
            }
            komaViews[i].setColorFilter(Color.argb(50, 0, 0, 0));
            komaViews[i].setBackgroundColor(Color.argb(50, 0, 0, 0));

        }

    }

    /**
     * 盤面を明るくするメソッド
     */
    private void lightUpAllCell(){
        for (int i = 0; i < CELL_SIZE; i++){
            if (i == 3 || i == 51) {
                continue;
            }
            komaViews[i].setColorFilter(Color.argb(0, 0, 0, 0));
            komaViews[i].setBackgroundColor(Color.argb(0, 0, 0, 0));
        }
    }

    /**
     * cellsを明るくするメソッド
     * @param cells
     */
    private void lightUpCell(ArrayList<Integer> cells) {
        for (Integer cell : cells) {
            if (cell == 3 || cell == 51)
                cell--;
            komaViews[cell].setColorFilter(Color.argb(0, 0, 0, 0));
            komaViews[cell].setBackgroundColor(Color.argb(0, 0, 0, 0));
        }
    }


    /**
     * 座標からセル番号を求める
     * @param x
     * @param y
     * @return -1は盤面外
     */
    private int pointToCell (int x, int y){
        int cell = 0;
        
        int col = (int)Math.round((double)x / (double)cellWidth);
        int row = (int)Math.round((double)y / (double)cellWidth);

        cell = row * 6 + col;

        if (cell == 3 || cell == 51){
            cell--;
        }

        if (cell < 0 || CELL_SIZE < cell) {
            return -1;
        }

        return cell;
    }


    /**
     * 駒の勝敗を判定するメソッド
     * @param friend 移動元のセル番号
     * @param enemy  移動先のセル番号
     * @return -1:負け 0:引き分け 1:勝ち TYPE_ROAD:道
     */
    private int judge(int friend, int enemy){
        if (enemy == TYPE_ROAD) {
            return TYPE_ROAD;
        }
        return strengthChart[friend][enemy];
    }


    /**
     * ゲーム終了のダイアログを表示するメソッド
     * TODO ゲーム終了後，bluetoothを切断したい
     * @param msg 勝者・敗者用メッセージ
     */
    private void finishGame(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // アラートダイアログのタイトルを設定します
        alertDialogBuilder.setTitle("軍人将棋");
        // アラートダイアログのメッセージを設定します
        alertDialogBuilder.setMessage(msg);
        // アラートダイアログの肯定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
        alertDialogBuilder.setPositiveButton("メニューに戻る",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
//        // アラートダイアログの否定ボタンがクリックされた時に呼び出されるコールバックリスナーを登録します
//        alertDialogBuilder.setNegativeButton("いいえ",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                    }
//                });

        // アラートダイアログのキャンセルが可能かどうかを設定します
        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        // アラートダイアログを表示します
        alertDialog.show();
    }


    /**
     * bluetoothのメソッド
     * 修正した部分にはFIXEDアノテーションをつけとく
     */

    private void setupBluetooth(){
        bluetoothService = new BluetoothService(this, mHandler);

        mOutStringBuffer = new StringBuffer("");
    }

    // The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));

                            break;
                        /**
                         * FIXED サーバ側のみ発火する
                         */
                        case BluetoothService.STATE_CONNECTING:
                            // サーバ側を先攻に設定
                            isFirst = true;
                            isTurn = true;
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    // mConversationArrayAdapter.add("Me:  " + writeMessage+"送ったメッセージ");

                    break;
                /**
                 *  FIXED データを受信したときに発火
                 *  送信時にオリジナルのオプションヘッダをつけているので，パースして分岐
                 */
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    // mConversationArrayAdapter.add(mConnectedDeviceName+":  " + readMessage+"受け取ったメッセージ");

                    // FIXED
                    // option_header/dataで送信しているのでパース
                    String[] arg = readMessage.split("/");
                    int opt = Integer.parseInt(arg[0]);
                    switch (opt) {
                        // 敵の配置情報の受信 : データはarrayList.toString("[,,,]")の形になっている
                        case MESSAGE_OPTION_ENEMIES:
                            // stringからarrayListに変換
                            enemiesInitList = stringToIntArray(arg[1]);
                            // 敵軍の駒配置可能
                            readyEnemy = true;
                            // 盤面が生成できていれば敵軍の駒も配置
                            if (readyBattleField){
                                arrangeEnemies(enemiesInitList);
                                if (isFirst) {
                                    Toast.makeText(getApplicationContext(), "あなたのターンです", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getApplicationContext(), "あいてのターンです", Toast.LENGTH_SHORT).show();
                                }
                            }
                            break;
                        // 駒の移動(対戦) : "judge/type/from/to"の形になっている
                        case MESSAGE_OPTION_BATTLE:
                            int koma = Integer.parseInt(arg[2]);
                            int from = Integer.parseInt(arg[3]);
                            int to = Integer.parseInt(arg[4]);
                            int judge = Integer.parseInt(arg[1]);
                            battle(koma, from, to, judge, IS_ENEMY);
                            // 自分のターンにする
                            isTurn = true;
                            Toast.makeText(getApplicationContext(), "あなたのターンです", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            break;
                    }
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    // コネクション確立（クライアント）
                    startBattleConfig(mConnectedDeviceName);

//                    Toast.makeText(getApplicationContext(), "Connected to "
//                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    /**
     * Sends a message.
     * @param message  A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (bluetoothService.getState() != BluetoothService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send-----------------------------------送ったときのしょり
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothChatService to write
            byte[] send = message.getBytes();
            bluetoothService.write(send);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            //mOutEditText.setText(mOutStringBuffer+"テキストエリア");
            //setContentView(R.layout.layout);
        }
    }


    private final void setStatus(int resId) {
//        final ActionBar actionBar = getActionBar();
//        actionBar.setSubtitle(resId);
    }

    private final void setStatus(CharSequence subTitle) {
//        final ActionBar actionBar = getActionBar();
//        actionBar.setSubtitle(subTitle);
    }


    public void pushBluetoothButton(View v){
        Intent serverIntent = null;
        switch (v.getId()){
            case R.id.secure_connect_scan:
                // Launch the DeviceListActivity to see devices and do scan
                serverIntent = new Intent(this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                break;
            case R.id.insecure_connect_scan:
                // Launch the DeviceListActivity to see devices and do scan
                serverIntent = new Intent(this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                break;
            default:
                break;
        }
    }



//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        Intent serverIntent = null;
//        switch (item.getItemId()) {
//            case R.id.secure_connect_scan:
//                // Launch the DeviceListActivity to see devices and do scan
//                serverIntent = new Intent(this, DeviceListActivity.class);
//                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
//                return true;
//            case R.id.insecure_connect_scan:
//                // Launch the DeviceListActivity to see devices and do scan
//                serverIntent = new Intent(this, DeviceListActivity.class);
//                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
//                return true;
//            case R.id.discoverable:
//                // Ensure this device is discoverable by others
//                ensureDiscoverable();
//                return true;
//        }
//        return false;
//    }

    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }


    /**
     * 遷移先から戻ってきたときに呼び出されるメソッド．
     * FIXED BATLLE_START: 駒の並べ替え画面から戻ってきたときにゲームを開始する
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupBluetooth();
                } else {
                    // User did not enable Bluetooth or an error occurred

                    Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                    finish();
                }
            // FIXED
            case BATTLE_START:
                if (resultCode == Activity.RESULT_OK) {
                    // 並び替えた駒の情報を遷移先のActicityから取得
                    ArrayList<Integer> friends = data.getIntegerArrayListExtra("koma");
                    // 盤面を生成
                    initializeFieldOfView();
                    // 友軍の駒を配置
                    arrangeFriends(friends);
                    // 相手に，駒情報をオプションヘッダ付きで送信
                    sendMessage(MESSAGE_OPTION_ENEMIES + "/" + friends.toString());

                }

                break;
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        bluetoothService.connect(device, secure);
    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (bluetoothService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (bluetoothService.getState() == BluetoothService.STATE_NONE) {
                // Start the Bluetooth chat services
                bluetoothService.start();
            }
        }


    }

    public synchronized void onRestart() {
        super.onRestart();
    }

    /**
     * bluetooth接続(ペアリング)完了後，ダイアログを表示
     * TODO いいえを押したとき，ペアリングを解除
     * @param enemyName
     */
    private void startBattleConfig(String enemyName){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("軍人将棋");
        String army = "後攻";
        if (isFirst) {
            army = "先攻";
        }
        alertDialogBuilder.setMessage(enemyName+"と対戦しますか あなたが" + army + "です");
        final String finalArmy = army;
        alertDialogBuilder.setPositiveButton("はい",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Activity_1.this, ArrangeKomaActivity.class);
                        intent.putExtra( "activity", 1);
                        intent.putExtra("army", finalArmy);
                        // 遷移先から返却されてくる際の識別コード
                        // 返却値を考慮したActivityの起動を行う
                        startActivityForResult(intent, BATTLE_START);
                    }
                });
        alertDialogBuilder.setNegativeButton("いいえ",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        finish();
                    }
                });

        alertDialogBuilder.setCancelable(true);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }



    /**
     * arrayList.toString()をarrayListに戻す．
     * "[1,2,3]" -> arrayList
     * @param string
     * @return
     */
    private ArrayList<Integer> stringToIntArray(String string) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();

        char[] charArray = string.toCharArray();
        String num = "";

        for (char c : charArray) {
            if (c == '['  || c == ' ') {
               continue;
            }
            if (c == ',' || c == ']'){
                arrayList.add(Integer.parseInt(num));
                num = "";
                continue;
            }
            num += c;
        }
        return arrayList;
    }

}
