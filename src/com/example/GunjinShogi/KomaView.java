package com.example.GunjinShogi;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by fujisho on 2014/10/03.
 */
public class KomaView extends ImageView {


    // 0~15 将佐尉飛車騎工ス地
    private int type;
    private int army;


    private int strengthChart[][] = {
            { 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,-1, 0}, // 0 大将
            {-1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0}, // 1 中将
            {-1,-1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0}, // 2 少将
            {-1,-1,-1, 0, 1, 1, 1, 1, 1,-1,-1, 1, 1, 1, 0}, // 3 大佐
            {-1,-1,-1,-1, 0, 1, 1, 1, 1,-1,-1, 1, 1, 1, 0}, // 4 中佐
            {-1,-1,-1,-1,-1, 0, 1, 1, 1,-1,-1, 1, 1, 1, 0}, // 5 少佐
            {-1,-1,-1,-1,-1,-1, 0, 1, 1,-1,-1, 1, 1, 1, 0}, // 6 大尉
            {-1,-1,-1,-1,-1,-1,-1, 0, 1,-1,-1, 1, 1, 1, 0}, // 7 中尉
            {-1,-1,-1,-1,-1,-1,-1,-1, 0,-1,-1, 1, 1, 1, 0}, // 8 少尉
            {-1,-1,-1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1}, // 9 飛行機
            {-1,-1,-1, 1, 1, 1, 1, 1, 1,-1, 0, 1,-1, 1, 0}, //10 タンク
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 1, 1, 0}, //11 騎兵
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 1,-1, 0, 1, 1}, //12 工兵
            { 1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0, 0}, //13 スパイ
            { 0, 0, 0, 0, 0, 0, 0, 0, 0,-1, 0, 0,-1, 0, 0}, //14 地雷
            {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}  //15 軍旗
    };

    public KomaView(Context context) {
        super(context);
    }

    public KomaView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KomaView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setArmy(int army) {

        this.army = army;

    }

    public int getArmy() {
        return army;
    }



    public int getMovableType() {
        int movable = 0;
        switch (type) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 13:
                movable = 1;
                break;
            case 9:
                movable = 2;
                break;
            case 10:
            case 11:
                movable = 3;
                break;
            case 12:
                movable = 4;
                break;
            case 14:
            case 15:
                movable = 5;
                break;
            default:
                break;
        }

        return movable;
    }
}
